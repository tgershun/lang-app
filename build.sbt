name := "lang-app"
organization := "ru.tg"

version := "0.1"

lazy val payment = project.in(file("payment"))
  .settings(
    scalaVersion := "2.12.11",
    scalacOptions := ScalacOptions.options,
    libraryDependencies ++= Dependencies.commonDependencies
  )

lazy val lang = project.in(file("lang"))
  .settings(
    scalaVersion := "2.12.11",
    scalacOptions := ScalacOptions.options,
    libraryDependencies ++= Dependencies.commonDependencies ++ Dependencies.langDependencies
  )
