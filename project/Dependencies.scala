import Versions._
import sbt._


object Dependencies {
  val http4s = Seq(
    "org.http4s" %% "http4s-dsl" % "0.21.4",
    "org.http4s" %% "http4s-blaze-server" % "0.21.4",
    "org.http4s" %% "http4s-blaze-client" % "0.21.4",
    "org.http4s" %% "http4s-circe" % "0.21.2",
  )

  val circe = Seq(
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-generic-extras" % circeVersion,
  )

  val postgres = Seq(
    "org.tpolecat" %% "doobie-core" % "0.8.8",
    "org.tpolecat" %% "doobie-postgres" % "0.8.8",
    "org.tpolecat" %% "doobie-hikari" % "0.8.8",
    "org.postgresql" % "postgresql" % "9.4-1203-jdbc4",
    "org.tpolecat" %% "doobie-specs2" % "0.9.0" % Test,
    "org.tpolecat" %% "doobie-scalatest" % "0.9.0" % Test,
    "com.opentable.components" % "otj-pg-embedded" % "0.13.3" % Test,
  )

  val logging = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  )

  val cats = Seq(
    "org.typelevel" %% "cats-effect" % "1.3.0",
  )

  val config = Seq(
    "com.github.pureconfig" %% "pureconfig" % "0.12.3",
  )

  val enumeratum = Seq(
    "com.beachape" %% "enumeratum-circe" % "1.5.23",
    "com.beachape" %% "enumeratum" % "1.5.15",
  )  
  
  val test = Seq(
    "org.scalatest" %% "scalatest" % "3.0.8" % Test,
    "org.scalamock" %% "scalamock" % "4.4.0" % Test
  )

  val commonDependencies = http4s ++ circe ++ logging ++ cats ++ config
  val langDependencies = postgres ++ test ++ enumeratum
}

object Versions {
  val circeVersion = "0.13.0"

}