object ScalacOptions {
  val options = Seq(
    "-Ywarn-dead-code",
  // Warn when dead code is identified.
  "-Ywarn-value-discard",
  // Warn when non-Unit expression results are unused.
  "-unchecked",
  "-Xfatal-warnings",
  "-feature",
  "-deprecation",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Ypartial-unification"
  )
}
