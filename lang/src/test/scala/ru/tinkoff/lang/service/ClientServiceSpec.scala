package ru.tinkoff.lang.service

import java.time.LocalDate
import java.util.UUID

import cats.effect.{ContextShift, IO}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.tinkoff.lang.entity.{Client, Gender, Language, Lesson, Role, Status}
import ru.tinkoff.lang.exceptions.ServiceExceptions.{InvalidCard, LessonAlreadyPaid, NotAcceptedLesson, PaymentException, UnauthorizedUser}

// initial test data in init.sql
class ClientServiceSpec extends AnyFlatSpec with Matchers {
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)
  //avoid conflict better. shouldn't start up different postgres dbs
  implicit val transactor: Transactor[IO] = EmbeddedPostgres.startupLocalDatabase(8083).unsafeRunSync()

  val failurePaymentService = new PaymentService {
    override def charge(card: UUID, amount: Int): IO[Unit] = IO.raiseError(PaymentException("Payment exception"))

    override def checkCard(cardId: UUID): IO[Unit] = IO.raiseError(InvalidCard("Invalid card"))
  }
  val fClientService = new ClientServiceImpl(transactor, failurePaymentService, new AuthServiceImpl)


  "register client" should "throw InvalidCard exception" in {
    val client = Client(None, "FN", "LN", Language.English, Seq(Language.French), UUID.randomUUID(), Some(Gender.Male), None)
    assertThrows[InvalidCard] {
      fClientService.register(client).unsafeRunSync()
    }
  }

  "get client by id 10" should "return client" in {
    val client = Client(Some(10), "SomefName10", "lName", Language.English, Seq(Language.English), UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), Some(Gender.Male), None)
    val res = fClientService.getById(10, 11, Role.Client).unsafeRunSync()
    res shouldBe client
  }

  "pay for lesson" should "throw assertThrows[PaymentException] " in {
    assertThrows[PaymentException] {
      fClientService.payForLesson(30, 10, Role.Client).unsafeRunSync()
    }
  }

  "pay for lesson" should "throw assertThrows[UnauthorizedUser] " in {
    assertThrows[UnauthorizedUser] {
      fClientService.payForLesson(30, 11, Role.Client).unsafeRunSync()
    }
  }

  "pay for lesson" should "throw assertThrows[NotAcceptedLesson] " in {
    assertThrows[NotAcceptedLesson] {
      fClientService.payForLesson(10, 10, Role.Client).unsafeRunSync()
    }
  }

  "pay for lesson" should "throw assertThrows[LessonAlreadyPaid] " in {
    assertThrows[LessonAlreadyPaid] {
      fClientService.payForLesson(20, 10, Role.Client).unsafeRunSync()
    }
  }

  val sPaymentService = new PaymentService {
    override def charge(card: UUID, amount: Int): IO[Unit] = IO.unit

    override def checkCard(cardId: UUID): IO[Unit] = IO.unit
  }
  val sClientService = new ClientServiceImpl(transactor, sPaymentService, new AuthServiceImpl)

  "register client" should "return client" in {
    val client = Client(None, "FN", "LN", Language.English, Seq(Language.French), UUID.randomUUID(), Some(Gender.Male), None)
    val res = sClientService.register(client).unsafeRunSync()
    res.copy(id = None) shouldBe client
  }

  "pay for lesson" should "return lesson" in {
    val lesson = Lesson(Some(30), 10, LocalDate.of(2020,1,1), 7, 10, Status.Accepted, true, Some("someComment"))
    val res = sClientService.payForLesson(30, 10, Role.Client).unsafeRunSync()
    res shouldBe lesson
  }

}
