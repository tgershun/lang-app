package ru.tinkoff.lang.service.queries

import java.time.LocalDate
import java.util.UUID

import cats.effect.{ContextShift, IO}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import org.specs2.mutable.Specification
import ru.tinkoff.lang.entity.{Client, Gender, Language, Professor, Score, Status}
import ru.tinkoff.lang.service.EmbeddedPostgres

class AnalysisTestSpec extends Specification with doobie.specs2.IOChecker {
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)
  //avoid conflict better. shouldn't start up different postgres dbs
  implicit val transactor: Transactor[IO] = EmbeddedPostgres.startupLocalDatabase(8084).unsafeRunSync()


  val client = Client(Some(1), "FN", "LN", Language.English, Seq(Language.French), UUID.randomUUID(), Some(Gender.Male), None)
  check(ClientQueries.insertClient(client))
  check(ClientQueries.updateClient(1, client))
  check(ClientQueries.getClientByIdQuery(1))
  check(ClientQueries.blockClientQuery(1))

  val professor = Professor(Some(1), "Some fn1", "LN1", Language.English, Seq(Language.French, Language.English), true, UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), 700, Some(Gender.Female), None, None)
  val score = Score(None, None, 5, Some("He's cool!"))

  check(ProfessorQueries.insertProfessor(professor))
  check(ProfessorQueries.updateProfessor(1, professor))
  check(ProfessorQueries.getProfessorByIdQuery(1))
  check(ProfessorQueries.lessonsCountQuery(1, 2))
  check(ProfessorQueries.upsertVirtMoneyInfo(1, 700))
  check(ProfessorQueries.insertScoreQuery(1, 1, score))
  check(ProfessorQueries.listProfessorsQuery(Language.English, None, None, None, None, None))

  check(LessonQueries.markLessonAsPaidQuery(1))
  check(LessonQueries.getLessonByIdQuery(1))
  check(LessonQueries.getScheduleQuery(1, LocalDate.now()))
  check(LessonQueries.setLessonStatusQuery(1, Status.Accepted))
  check(LessonQueries.requestLessonQuery(1, LocalDate.now, 3, 4, true, Status.Declined, Some("comment")))

  check(ScoreQueries.listScoresQuery(1))
  check(ScoreQueries.updateProfessorsScore(2))
}