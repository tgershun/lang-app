package ru.tinkoff.lang.service

import java.time.LocalDate
import java.util.UUID

import cats.effect.{ContextShift, IO}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.tinkoff.lang.entity.{Gender, Language, Lesson, Professor, Role, Score, Status}
import ru.tinkoff.lang.exceptions.ServiceExceptions.{AlreadyRated, InvalidCard, NoLessonsPassed, PaymentException, TakenTimeSlot, UnauthorizedUser}

// initial test data in init.sql
class ProfessorServiceSpec extends AnyFlatSpec with Matchers {
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)
  //avoid conflict better. shouldn't start up different postgres dbs
  implicit val transactor: Transactor[IO] = EmbeddedPostgres.startupLocalDatabase(8082).unsafeRunSync()

  val failurePaymentService = new PaymentService {
    override def charge(card: UUID, amount: Int): IO[Unit] = IO.raiseError(PaymentException("Payment exception"))

    override def checkCard(cardId: UUID): IO[Unit] = IO.raiseError(InvalidCard("Invalid card"))
  }
  val professorService = new ProfessorServiceImpl(transactor, failurePaymentService, new AuthServiceImpl)

  "get list for English" should "return list of 6" in {
    val professors = professorService.list(Language.English, None, None, None, None, None, 0, 10).unsafeRunSync()

    professors.size shouldBe 6
  }

  "get list of nativeSpeakers" should "return list of 6" in {
    val professors = professorService.list(Language.English, None, Some(true), None, None, None, 0, 10).unsafeRunSync()

    professors.size shouldBe 4
  }

  "get list of nativeSpeakers with price < 500" should "return list of 6" in {
    val professors = professorService.list(Language.English, None, Some(true), None, Some(300), None, 0, 10).unsafeRunSync()

    professors.size shouldBe 2
  }

  "update professor" should " throw InvalidCard exception" in {
    val professor = Professor(Some(11), "Some fn1", "LN1", Language.English, Seq(Language.French, Language.English), true, UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), 700, Some(Gender.Female), None, None)

    assertThrows[InvalidCard] {
      professorService.update(11, professor, 11, Role.Instructor).unsafeRunSync()
    }
  }

  "schedule for p 10 on 2020-01-01" should "give 3 lessons" in {
    val lessons = professorService.getSchedule(10, LocalDate.of(2020, 1, 1), 10, Role.Instructor).unsafeRunSync()
    lessons.size shouldBe 3
  }

  "approve lesson 10" should "give unauthorized for user 13" in {
    assertThrows[UnauthorizedUser] {
      professorService.approveLesson(10, 13, Role.Instructor).unsafeRunSync()
    }
  }

  "approve lesson 10" should "return approved lesson" in {
    val lesson = Lesson(Some(10), 10, LocalDate.of(2020, 1, 1), 3, 10, Status.Accepted, false, Some("someComment"))
    val res = professorService.approveLesson(10, 10, Role.Instructor).unsafeRunSync()
    res shouldBe lesson
  }

  "request lesson timeSlot 3" should "throw TakenTimeSlot exception" in {
    assertThrows[TakenTimeSlot] {
      professorService.requestLesson(10, LocalDate.of(2020, 1, 1), 3, 11, Role.Client).unsafeRunSync()
    }
  }

  "request lesson timeSlot 8" should "return new lesson" in {
    val lesson = Lesson(None, 10, LocalDate.of(2020, 1, 2), 8, 11, Status.Requested, false, None)
    val res = professorService.requestLesson(10, LocalDate.of(2020, 1, 2), 8, 11, Role.Client).unsafeRunSync()
    res.copy(id = None) shouldBe lesson
  }

  "list scores for p 10" should "return 2 scores" in {
    val scores = professorService.listScores(13, 0, 40, 11, Role.Client).unsafeRunSync()
    scores.size shouldBe 2
  }

  "list scores for p 11" should "return empty list" in {
    val scores = professorService.listScores(11, 0, 40, 11, Role.Client).unsafeRunSync()
    scores.size shouldBe 0
  }

  "rate p 10" should "return score" in {
    val score = Score(Some(10), Some(10), 5, Some("He's cool!"))
    val res = professorService.rate(10, Score(None, None, 5, Some("He's cool!")), 10, Role.Client).unsafeRunSync()
    res shouldBe score
  }

  "rate p 10" should "throw NoLessonsPassed for user 13" in {
    assertThrows[NoLessonsPassed] {
      professorService.rate(10, Score(None, None, 5, Some("He's cool!")), 13, Role.Client).unsafeRunSync()
    }
  }

  "rate p 10" should "throw AlreadyRated for user 10" in {
    assertThrows[AlreadyRated] {
      professorService.rate(10, Score(None, None, 5, Some("He's cool!")), 10, Role.Client).unsafeRunSync()
    }
  }

}
