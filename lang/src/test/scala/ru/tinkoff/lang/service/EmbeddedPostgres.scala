package ru.tinkoff.lang.service
import cats.effect.{Blocker, ContextShift, IO, Resource}
import com.opentable.db.postgres.embedded.{EmbeddedPostgres => EmbeddedPostgresDatabase}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import javax.sql.DataSource

object EmbeddedPostgres {
  implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContexts.synchronous)

  def startupLocalDatabase(port: Int): IO[Transactor[IO]] = {
    for {
      dataSource <- IO(startupPostgresDatabase(port))
      _ <- runInitScript(dataSource)
      transactor <- IO(createTransactor(dataSource))
    } yield transactor
  }

  private def startupPostgresDatabase(port: Int) =
    EmbeddedPostgresDatabase
      .builder()
      .setPort(port)
      .start
      .getPostgresDatabase


  private def runInitScript(dataStore: DataSource) =
    for {
      init <- initScript
      _ <- IO(dataStore.getConnection.createStatement().execute(init))
    } yield ()



  private val acquire = IO {
    scala.io.Source.fromResource("init.sql")
  }
  private val initScript = {
    Resource.fromAutoCloseable(acquire).use(source => IO(source.getLines.mkString("\n")))
  }

  def createTransactor(dataSource: DataSource): Transactor[IO] =
    Transactor.fromDataSource[IO](
      dataSource,
      ExecutionContexts.synchronous,
      Blocker.liftExecutionContext(ExecutionContexts.synchronous)
    )

}
