package ru.tinkoff.lang.controller

import java.time.LocalDate
import java.util.UUID

import cats.data.Kleisli
import cats.effect.IO
import io.circe.Json
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.{Request, Response, Status}
import org.scalatest.flatspec.AnyFlatSpec
import ru.tinkoff.lang.entity._
import ru.tinkoff.lang.exceptions.ServiceExceptions.{AlreadyRated, InvalidCard, LessonNotFound, NoLessonsPassed, ProfessorException, TakenTimeSlot, UserNotFound}
import ru.tinkoff.lang.exceptions.{ErrorResponse, HttpErrorHandler, ProfessorHandler}
import ru.tinkoff.lang.exceptions.ErrorResponse._
import ru.tinkoff.lang.service.{AuthServiceImpl, ProfessorService}

class ProfessorControllerSpec extends AnyFlatSpec
  with CommonAssertion
  with MocksForProfessorController
  with Http4sDsl[IO]
  with Http4sClientDsl[IO] {

  "GET /professor/{id}?userId=1&role=Instructor" should "return professor by id" in {
    check[Json](getByIdProfessorResponse, Status.Ok, Some(someProfessor1.asJson))
  }

  "GET /professor/{id}?userId=1&role=Instructor" should "return notFound" in {
    check[Json](noneProfessorResponse, Status.NotFound, Some(ErrorResponse(ProfessorNotFoundMessage).asJson))
  }

  "POST /professor/register" should "register given user" in {
    check[Json](registerProfessorResponse, Status.Ok, Some(someProfessor1.asJson))
  }

  "POST /professor/register" should "register return PaymentRequired status" in {
    check[Json](registerProfessorWIthInvalidCardResponse, Status.PaymentRequired, Some(ErrorResponse(InvalidCardMessage).asJson))
  }

  "POST /professor/update/1?userId=1&role=Instructor" should "update given user" in {
    check[Json](updateProfessorResponse, Status.Ok, Some(someProfessor1.asJson))
  }

  "POST /professor/update/1?userId=2&role=Instructor" should "should give unauthorized" in {
    check[Json](updateProfessorUnauthorizedResponse, Status.Forbidden, Some(ErrorResponse(authService.unauthorizedMessage).asJson))
  }

  "GET /professor/list" should "should give list of professors" in {
    check[Json](listResponse, Status.Ok, Some(Seq(someProfessor1, someProfessor2).asJson))
  }

  "GET /professor/list" should "should return empty list" in {
    check[Json](emptyListResponse, Status.Ok, Some(Seq.empty[Professor].asJson))
  }

  "POST /professor/approveLesson" should "should return lesson" in {
    check[Json](approveLessonResponse, Status.Ok, Some(someLesson1.asJson))
  }

  "GET /professor/getLesson?lId=2&userId=2&role=Instructor" should "should return lesson" in {
    check[Json](getLessonResponse, Status.Ok, Some(someLesson2.asJson))
  }

  "GET /professor/getLesson?lId=400&userId=2&role=Instructor" should "should return not found" in {
    check[Json](lessonNotFoundResponse, Status.NotFound, Some(ErrorResponse(LessonNotFoundMessage).asJson))
  }

  "GET /professor/1/schedule?date=2020-05-30&userId=1&role=Instructor" should "should return list of lessons" in {
    check[Json](scheduleResponse, Status.Ok, Some(Seq(someLesson1, someLesson2).asJson))
  }

  "GET /professor/1/schedule?date=2020-05-30&userId=1&role=Instructor" should "should return empty list" in {
    check[Json](scheduleEmptyResponse, Status.Ok, Some(Seq.empty[Lesson].asJson))
  }

  "POST /professor/1/requestLesson?date=2020-05-30&userId=1&role=Instructor&slotN=1" should "should return lesson" in {
    check[Json](requestLessonResponse, Status.Ok, Some(someLesson1.asJson))
  }

  "POST /professor/1/requestLesson?date=2020-05-30&userId=1&role=Instructor&slotN=10" should "should return lesson" in {
    check[Json](requestTakenTimeslotResponse, Status.Conflict, Some(ErrorResponse(NotAvailableTimeslotMessage).asJson))
  }

  "POST /professor/1/rate?userId=1&role=Instructor" should "should return score" in {
    check[Json](rateResponse, Status.Ok, Some(someScore1.asJson))
  }

  "POST /professor/1/rate?userId=2&role=Instructor" should "should return no lessons passed message" in {
    check[Json](noLessonsPassedResponse, Status.BadRequest, Some(ErrorResponse(NoLessonsPassedMessage).asJson))
  }

  "POST /professor/1/rate?userId=3&role=Instructor" should "should return already rated message" in {
    check[Json](alreadyRatedResponse, Status.Conflict, Some(ErrorResponse(AlreadyRatedMessage).asJson))
  }

  "GET /professor/1/listScores?userId=3&role=Instructor&page=0&size=10" should "should return list of scores" in {
    check[Json](scoreListResponse, Status.Ok, Some(Seq(someScore1).asJson))
  }

  "GET /professor/2/listScores?userId=3&role=Instructor&page=0&size=10" should "should return empty list of scores" in {
    check[Json](emptyScoreListResponse, Status.Ok, Some(Seq.empty[Score].asJson))
  }

  implicit val professorErrorHandler: HttpErrorHandler[ProfessorException] = new ProfessorHandler()

  private val professorControllerRouter: Kleisli[IO, Request[IO], Response[IO]] = Router(
    "/professor" -> new ProfessorController(mockedProfessorService).routes).orNotFound


  val getByIdProfessorResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/1?userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val noneProfessorResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/2?userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val registerProfessorResponse: IO[Response[IO]] = for {
    uri <- POST(someProfessor1.asJson, uri"/professor/register")
    response <- professorControllerRouter.run(uri)
  } yield response

  val registerProfessorWIthInvalidCardResponse: IO[Response[IO]] = for {
    uri <- POST(invalidCardProfessor.asJson, uri"/professor/register")
    response <- professorControllerRouter.run(uri)
  } yield response

  val updateProfessorResponse: IO[Response[IO]] = for {
    uri <- POST(someProfessor1.asJson, uri"/professor/update/1?userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val updateProfessorUnauthorizedResponse: IO[Response[IO]] = for {
    uri <- POST(someProfessor1.asJson, uri"/professor/update/2?userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val listResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/list?userId=1&role=Instructor&isNative=true&lang=English&page=0&size=10")
    response <- professorControllerRouter.run(uri)
  } yield response

  val emptyListResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/list?userId=1&role=Instructor&lang=English&page=0&size=10")
    response <- professorControllerRouter.run(uri)
  } yield response

  val approveLessonResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/professor/approveLesson?lId=1&userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val getLessonResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/getLesson?lId=2&userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val lessonNotFoundResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/getLesson?lId=400&userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val scheduleResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/1/schedule?date=2020-05-30&userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val scheduleEmptyResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/3/schedule?date=2020-05-30&userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val requestLessonResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/professor/1/requestLesson?date=2020-05-30&userId=1&role=Instructor&slotN=1")
    response <- professorControllerRouter.run(uri)
  } yield response

  val requestTakenTimeslotResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/professor/1/requestLesson?date=2020-05-30&userId=1&role=Instructor&slotN=10")
    response <- professorControllerRouter.run(uri)
  } yield response

  val rateResponse: IO[Response[IO]] = for {
    uri <- POST(someScore1.asJson, uri"/professor/1/rate?userId=1&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val noLessonsPassedResponse: IO[Response[IO]] = for {
    uri <- POST(someScore1.asJson, uri"/professor/1/rate?userId=2&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val alreadyRatedResponse: IO[Response[IO]] = for {
    uri <- POST(someScore1.asJson, uri"/professor/1/rate?userId=3&role=Instructor")
    response <- professorControllerRouter.run(uri)
  } yield response

  val scoreListResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/1/listScores?userId=3&role=Instructor&page=0&size=10")
    response <- professorControllerRouter.run(uri)
  } yield response

  val emptyScoreListResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/professor/2/listScores?userId=3&role=Instructor&page=0&size=10")
    response <- professorControllerRouter.run(uri)
  } yield response
}

sealed trait MocksForProfessorController {
  val someProfessor1 = Professor(Some(1), "Some fn1", "LN1", Language.English, Seq(Language.French, Language.English), true, UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), 700, Some(Gender.Female), None, None)
  val someProfessor2 = Professor(Some(2), "Some fn2", "LN2", Language.English, Seq(Language.French, Language.English), true, UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), 1000, Some(Gender.Female), None, None)
  val invalidCardProfessor = Professor(Some(1), "Some fn1", "LN1", Language.English, Seq(Language.French, Language.English), true, UUID.fromString("b83c0138-64ab-4104-87ba-e32dc2e13914"), 700, Some(Gender.Female), None, None)

  val someLesson1 = Lesson(Some(1), 1, LocalDate.now, 3, 3, ru.tinkoff.lang.entity.Status.Accepted, false, None)
  val someLesson2 = Lesson(Some(2), 1, LocalDate.now, 4, 3, ru.tinkoff.lang.entity.Status.Accepted, false, None)

  val someScore1 = Score(None, None, 5, Some("He's cool!"))

  val InvalidCardMessage = "Card is invalid"
  val ProfessorNotFoundMessage = "Professor wasn't found"
  val PaymentExceptionMessage = "Payment exception"
  val LessonNotFoundMessage = "Lesson wasn't found"
  val NotAvailableTimeslotMessage = "Timeslot is not available"
  val NoLessonsPassedMessage = "No lessons passed"
  val AlreadyRatedMessage = "Already rated"

  val authService = new AuthServiceImpl
  val mockedProfessorService: ProfessorService = new ProfessorService {
    override def register(professor: Professor): IO[Professor] =
      if (professor.cardId == UUID.fromString("b83c0138-64ab-4104-87ba-e32dc2e13914")) {
        IO.raiseError(InvalidCard(InvalidCardMessage))
      } else IO.pure(someProfessor1)

    override def update(id: Int, professor: Professor, userId: Int, role: Role): IO[Professor] =
      for {
        _ <- authService.checkPerms(role, Role.Instructor, id, Some(userId))
        res <- IO.pure(someProfessor1)
      } yield res

    override def getById(id: Int, userId: Int, role: Role): IO[Professor] =
      if (id == 1) {
        IO.pure(someProfessor1)
      } else {
        IO.raiseError(UserNotFound(ProfessorNotFoundMessage))
      }

    override def list(lang: Language, age: Option[Int], isNative: Option[Boolean], hasEducation: Option[Boolean], price: Option[Int], score: Option[BigDecimal], page: Int, pageSize: Int): IO[Seq[Professor]] =
      if (isNative.isDefined) {
        IO.pure(Seq(someProfessor1, someProfessor2))
      } else {
        IO.pure(Seq.empty[Professor])
      }

    override def approveLesson(lId: Int, userId: Int, role: Role): IO[Lesson] = {
      IO.pure(someLesson1)
    }

    override def getLesson(lId: Int, userId: Int, role: Role): IO[Lesson] = {
      if (lId == 2){
        IO.pure(someLesson2)
      } else {
        IO.raiseError(LessonNotFound(LessonNotFoundMessage))
      }
    }

    override def getSchedule(teacherId: Int, localDate: LocalDate, userId: Int, role: Role): IO[Seq[Lesson]] =
    {
      if(teacherId == 1) {
        IO.pure(Seq(someLesson1, someLesson2))
      } else {
        IO.pure(Seq.empty[Lesson])
      }
    }

    override def requestLesson(id: Int, date: LocalDate, timeSlot: Int, userId: Int, role: Role): IO[Lesson] =
    {
      if(timeSlot == 1) {
        IO.pure(someLesson1)
      } else {
        IO.raiseError(TakenTimeSlot(NotAvailableTimeslotMessage))
      }
    }

    override def rate(id: Int, score: Score, userId: Int, role: Role): IO[Score] =
      userId match {
        case 1 => IO.pure(someScore1)
        case 2 => IO.raiseError(NoLessonsPassed(NoLessonsPassedMessage))
        case _ => IO.raiseError(AlreadyRated(AlreadyRatedMessage))
      }

    override def listScores(id: Int, page: Int, pageSize: Int, userId: Int, role: Role): IO[Seq[Score]] = {
      if (id == 1) {
        IO.pure(Seq(someScore1))
      } else {
        IO.pure(Seq.empty[Score])
      }
    }
  }
}
