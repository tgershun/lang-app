package ru.tinkoff.lang.controller

import java.time.LocalDate
import java.util.UUID

import cats.data.Kleisli
import cats.effect.IO
import io.circe.Json
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.{Request, Response, Status}
import org.scalatest.flatspec.AnyFlatSpec
import ru.tinkoff.lang.entity._
import ru.tinkoff.lang.exceptions.ServiceExceptions.{ClientException, InvalidCard, LessonAlreadyPaid, NotAcceptedLesson, PaymentException, UserNotFound}
import ru.tinkoff.lang.exceptions.{ClientHandler, ErrorResponse, HttpErrorHandler}
import ru.tinkoff.lang.exceptions.ErrorResponse._
import ru.tinkoff.lang.service.{AuthServiceImpl, ClientService}


class ClientControllerSpec extends AnyFlatSpec
  with CommonAssertion
  with MocksForClientController
  with Http4sDsl[IO]
  with Http4sClientDsl[IO] {

  "GET /client/{id}?userId=1&role=Client" should "return client by id" in {
    check[Json](getByIdClientResponse, Status.Ok, Some(someClient1.asJson))
  }

  "GET /client/{id}?userId=1&role=Client" should "return notFound" in {
    check[Json](noneClientResponse, Status.NotFound, Some(ErrorResponse(ClientNotFoundMessage).asJson))
  }

  "POST /client/register" should "register given user" in {
    check[Json](registerClientResponse, Status.Ok, Some(someClient1.asJson))
  }

  "POST /client/register" should "register return PaymentRequired status" in {
    check[Json](registerClientWIthInvalidCardResponse, Status.PaymentRequired, Some(ErrorResponse(InvalidCardMessage).asJson))
  }

  "POST /client/update/1?userId=1&role=Client" should "update given user" in {
    check[Json](updateClientResponse, Status.Ok, Some(someClient1.asJson))
  }

  "POST /client/update/1?userId=2&role=Client" should "should give unauthorized" in {
    check[Json](updateClientUnauthorizedResponse, Status.Forbidden, Some(ErrorResponse(authService.unauthorizedMessage).asJson))
  }

  "POST /client/block/2?userId=1&role=Admin" should "block given user" in {
    check[Json](blockClientResponse, Status.Ok, Some(someClient1.asJson))
  }

  "POST /client/block/1?userId=2&role=Instructor" should "should give unauthorized" in {
    check[Json](instructorBlocksClientResponse, Status.Forbidden, Some(ErrorResponse(authService.unauthorizedMessage).asJson))
  }

  "POST /client/pay?lId=1&userId=100&role=Client" should "should be ok" in {
    check[Json](okPayForLessonResponse, Status.Ok, Some(someLesson.asJson))
  }

  "POST /client/pay?lId=1&userId=1&role=Client" should "should be ok" in {
    check[Json](notAcceptedLessonResponse, Status.BadRequest, Some(ErrorResponse(NotAcceptedLessonMessage).asJson))
  }

  "POST /client/pay?lId=1&userId=2&role=Client" should "should give BadRequest status" in {
    check[Json](lessonAlreadyPaidResponse, Status.BadRequest, Some(ErrorResponse(LessonAlreadyPaidMessage).asJson))
  }

  "POST /client/pay?lId=1&userId=3&role=Client" should "should give PaymentRequired status" in {
    check[Json](paymentExceptionResponse, Status.PaymentRequired, Some(ErrorResponse(PaymentExceptionMessage).asJson))
  }

  implicit val clientErrorHandler: HttpErrorHandler[ClientException] = new ClientHandler()

  private val clientControllerRouter: Kleisli[IO, Request[IO], Response[IO]] = Router(
    "/client" -> new ClientController(mockedClientService).routes).orNotFound


  val getByIdClientResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/client/1?userId=1&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val noneClientResponse: IO[Response[IO]] = for {
    uri <- GET(uri"/client/2?userId=1&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val registerClientResponse: IO[Response[IO]] = for {
    uri <- POST(someClient1.asJson, uri"/client/register")
    response <- clientControllerRouter.run(uri)
  } yield response

  val registerClientWIthInvalidCardResponse: IO[Response[IO]] = for {
    uri <- POST(invalidCardClient.asJson, uri"/client/register")
    response <- clientControllerRouter.run(uri)
  } yield response

  val updateClientResponse: IO[Response[IO]] = for {
    uri <- POST(someClient1.asJson, uri"/client/update/1?userId=1&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val updateClientUnauthorizedResponse: IO[Response[IO]] = for {
    uri <- POST(someClient1.asJson, uri"/client/update/2?userId=1&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val blockClientResponse: IO[Response[IO]] = for {
    uri <- POST(someClient1.asJson, uri"/client/block/2?userId=1&role=Admin")
    response <- clientControllerRouter.run(uri)
  } yield response

  val instructorBlocksClientResponse: IO[Response[IO]] = for {
    uri <- POST(someClient1.asJson, uri"/client/block/2?userId=1&role=Instructor")
    response <- clientControllerRouter.run(uri)
  } yield response

  val okPayForLessonResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/client/pay?lId=1&userId=100&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val notAcceptedLessonResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/client/pay?lId=1&userId=1&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val lessonAlreadyPaidResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/client/pay?lId=1&userId=2&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response

  val paymentExceptionResponse: IO[Response[IO]] = for {
    uri <- POST(uri"/client/pay?lId=1&userId=3&role=Client")
    response <- clientControllerRouter.run(uri)
  } yield response
}

sealed trait MocksForClientController {
  val someClient1 = Client(Some(1), "Some fn", "", Language.English, Seq(Language.French, Language.English), UUID.fromString("705bfe69-4eb9-4d91-9399-adbf39073bee"), Some(Gender.Female), None)
  val invalidCardClient = Client(Some(2), "Some fn2", "", Language.English, Seq(Language.French, Language.English), UUID.fromString("b83c0138-64ab-4104-87ba-e32dc2e13914"), Some(Gender.Female), None)
  val someLesson = Lesson(Some(2), 1, LocalDate.now, 3, 3, ru.tinkoff.lang.entity.Status.Accepted, false, None)

  val InvalidCardMessage = "Card is invalid"
  val ClientNotFoundMessage = "Client wasn't found"
  val NotAcceptedLessonMessage = "Lesson is not accepted"
  val LessonAlreadyPaidMessage = "Lesson is already paid"
  val PaymentExceptionMessage = "Payment exception"


  val authService = new AuthServiceImpl
  val mockedClientService: ClientService = new ClientService {
    override def register(client: Client): IO[Client] =
      if (client.cardId == UUID.fromString("b83c0138-64ab-4104-87ba-e32dc2e13914")) {
        IO.raiseError(InvalidCard(InvalidCardMessage))
      } else IO.pure(someClient1)

    override def update(id: Int, client: Client, userId: Int, role: Role): IO[Client] = {
      for {
        _ <- authService.checkPerms(role, Role.Client, id, Some(userId))
        res <- IO.pure(someClient1)
      } yield res
    }

    override def getById(id: Int, userId: Int, role: Role): IO[Client] =
      if (id == 1) {
        IO.pure(someClient1)
      } else {
        IO.raiseError(UserNotFound(ClientNotFoundMessage))
      }

    override def blockClient(id: Int, userId: Int, role: Role): IO[Client] =
      for {
        _ <- authService.checkPerms(role, Role.Admin, userId, None)
        res <- IO.pure(someClient1)
      } yield res

    override def payForLesson(lId: Int, userId: Int, role: Role): IO[Lesson] =
      userId match {
        case 1 => IO.raiseError(NotAcceptedLesson(NotAcceptedLessonMessage))
        case 2 => IO.raiseError(LessonAlreadyPaid(LessonAlreadyPaidMessage))
        case 3 => IO.raiseError(PaymentException(PaymentExceptionMessage))
        case _ => IO.pure(someLesson)
      }
  }
}
