CREATE TYPE gender AS ENUM ('female', 'male');

CREATE TABLE clients(
    id serial PRIMARY KEY  NOT NULL,
    first_name VARCHAR (50) NOT NULL,
    last_name VARCHAR (50) NOT NULL,
    native_language VARCHAR (50) NOT NULL,
    langs  VARCHAR(50)[] NOT NULL,
    card_id VARCHAR (50) NOT NULL,
    tariff VARCHAR (50) NOT NULL,
    is_active boolean NOT NUll,
    gender gender,
    birthday DATE
);

CREATE TABLE professors(
	id serial PRIMARY KEY  NOT NULL,
	first_name VARCHAR (50) NOT NULL,
	last_name VARCHAR (50) NOT NULL,
	native_language VARCHAR (50) NOT NULL,
	langs  VARCHAR(50)[] NOT NULL,
	card_id VARCHAR (50) NOT NULL,
	has_education boolean NOT NUll,
	price_per_lesson INT NOT NULL,
	score DECIMAL(3,2),
	is_active boolean NOT NUll,
	gender gender,
	birthday DATE
);

CREATE TABLE virt_money(
	pid int PRIMARY KEY  NOT NULL,
	money int NOT NULL,
	FOREIGN KEY (pid) REFERENCES professors(id)
);

CREATE TABLE scores(
	professor_id int NOT NULL,
	client_id int NOT NULL,
	score int NOT NULL,
	comment VARCHAR (50),
	FOREIGN KEY (professor_id) REFERENCES professors(id),
    FOREIGN KEY (client_id) REFERENCES clients(id),
	PRIMARY KEY(professor_id, client_id)
);

CREATE TABLE schedule(
    id SERIAL,
	professor_id int  NOT NULL,
	date DATE  NOT NULL,
	time_slot_id int NOT NULL,
	client_id int  NOT NULL,
	is_paid boolean NOT NULL,
	status VARCHAR (50) NOT NULL,
	comment VARCHAR (256),
	FOREIGN KEY (professor_id) REFERENCES professors(id),
	FOREIGN KEY (client_id) REFERENCES clients(id),
	PRIMARY KEY(professor_id, date, time_slot_id),
	UNIQUE (id)
);

insert into clients (id, first_name, last_name, native_language, langs, card_id, tariff, is_active, gender, birthday)
values (10, 'SomefName10', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 'Basic', true, 'male'::gender, null);
insert into clients (id, first_name, last_name, native_language, langs, card_id, tariff, is_active, gender, birthday)
values (11, 'SomefName11', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 'Basic', true, 'male'::gender, null);


insert into professors (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (10, 'SomefName10', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 800, 'male'::gender, null, true, 1, true);
insert into professors  (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (11, 'SomefName11', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 300, 'male'::gender, null, true, 5, true);
insert into professors  (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (12, 'SomefName12', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 500, 'male'::gender, null, false, 5, true);
insert into professors  (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (13, 'SomefName13', 'lName', 'German', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 300, 'male'::gender, null, false, 5, true);
insert into professors  (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (14, 'SomefName14', 'lName', 'French', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 700, 'male'::gender, null, true, 5, true);
insert into professors  (id, first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
values (15, 'SomefName15', 'lName', 'English', ARRAY['English'], '705bfe69-4eb9-4d91-9399-adbf39073bee', 300, 'male'::gender, null, true, 5, true);

insert into schedule (id, professor_id, date, time_slot_id, client_id, is_paid, status, comment)
values (10, 10, '2020-01-01', 3, 10, false, 'Requested', 'someComment');
insert into schedule (id, professor_id, date, time_slot_id, client_id, is_paid, status, comment)
values (20, 10, '2020-01-01', 4, 10, true, 'Accepted', 'someComment');
insert into schedule (id, professor_id, date, time_slot_id, client_id, is_paid, status, comment)
values (30, 10, '2020-01-01', 7, 10, false, 'Accepted', 'someComment');

insert into scores (professor_id, client_id, score, comment)
values (13, 10, 5, 'comment');
insert into scores (professor_id, client_id, score, comment)
values (13, 11, 5, 'comment');