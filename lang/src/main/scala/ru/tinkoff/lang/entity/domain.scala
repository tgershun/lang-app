package ru.tinkoff.lang.entity

import java.time.LocalDate
import java.util.UUID

import cats.effect.IO
import doobie.postgres.implicits.pgEnumStringOpt
import doobie.util.meta.Meta
import doobie.util.{Get, Put}
import enumeratum.{CirceEnum, Enum, EnumEntry}
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.semiauto.{deriveConfiguredDecoder, deriveConfiguredEncoder}
import org.http4s.circe.jsonOf
import doobie.postgres.implicits._

// for all dto
// better to have id generator to control ids and to simplify testing

final case class Admin(id: String,
                       role: Role = Role.Admin)


object Admin {

  import io.circe.generic.extras._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val encoder: Encoder[Admin] = deriveConfiguredEncoder
  implicit val decoder: Decoder[Admin] = deriveConfiguredDecoder
  implicit val configDecoder = jsonOf[IO, Admin]
}

final case class Client(id: Option[Int],
                        firstName: String,
                        lastName: String,
                        nativeLanguage: Language,
                        languagesToLearn: Seq[Language],
                        cardId: UUID,
                        gender: Option[Gender],
                        birthday: Option[LocalDate],
                        tariff: Tariff = Tariff.Basic,
                        isActive: Boolean = true,
                        role: Role = Role.Client)

object Client {

  import io.circe.generic.extras._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val encoder: Encoder[Client] = deriveConfiguredEncoder
  implicit val decoder: Decoder[Client] = deriveConfiguredDecoder
  implicit val configDecoder = jsonOf[IO, Client]
}


final case class Lesson(id: Option[Int],
                        professorId: Int,
                        date: LocalDate,
                        timeSlot: Int,
                        clientId: Int,
                        status: Status,
                        isPaid: Boolean,
                        comment: Option[String])

object Lesson {

  import io.circe.generic.extras._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val encoder: Encoder[Lesson] = deriveConfiguredEncoder
  implicit val decoder: Decoder[Lesson] = deriveConfiguredDecoder
  implicit val configDecoder = jsonOf[IO, Lesson]
}

final case class Professor(id: Option[Int],
                           firstName: String,
                           lastName: String,
                           nativeLanguage: Language,
                           languagesToTeach: Seq[Language],
                           hasEducation: Boolean,
                           cardId: UUID,
                           pricePerLesson: Int,
                           gender: Option[Gender],
                           birthday: Option[LocalDate],
                           score: Option[BigDecimal],
                           isActive: Boolean = true,
                           role: Role = Role.Instructor)

object Professor {

  import io.circe.generic.extras._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val encoder: Encoder[Professor] = deriveConfiguredEncoder
  implicit val decoder: Decoder[Professor] = deriveConfiguredDecoder
  implicit val configDecoder = jsonOf[IO, Professor]
}

final case class Score(pId: Option[Int], cId: Option[Int], score: Int, comment: Option[String])

object Score {

  import io.circe.generic.extras._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val encoder: Encoder[Score] = deriveConfiguredEncoder
  implicit val decoder: Decoder[Score] = deriveConfiguredDecoder
  implicit val configDecoder = jsonOf[IO, Score]
}


sealed trait Role extends EnumEntry

object Role extends CirceEnum[Role] with Enum[Role] {
  val values = findValues

  case object Admin extends Role

  case object Instructor extends Role

  case object Client extends Role

}


sealed trait Tariff extends EnumEntry

object Tariff extends CirceEnum[Tariff] with Enum[Tariff] {
  val values = findValues

  case object Premium extends Tariff

  case object Basic extends Tariff

}

sealed trait Language extends EnumEntry

object Language extends CirceEnum[Language] with Enum[Language] {
  val values = findValues

  case object English extends Language

  case object French extends Language

  case object Spanish extends Language

  case object Italian extends Language

  case object German extends Language

  case object Russian extends Language

  case object Portuguese extends Language

}

sealed trait Gender extends EnumEntry

object Gender extends CirceEnum[Gender] with Enum[Gender] {
  val values = findValues

  case object Male extends Gender

  case object Female extends Gender

  def toEnum(e: Gender): String =
    e match {
      case Male => "male"
      case Female => "female"
    }

  def fromEnum(s: String): Option[Gender] =
    Option(s) collect {
      case "male" => Male
      case "female" => Female
    }

}

sealed trait Status extends EnumEntry

object Status extends CirceEnum[Status] with Enum[Status] {
  val values = findValues

  case object Requested extends Status

  case object Accepted extends Status

  case object Declined extends Status

  case object Blocked extends Status

}

object DomainImplicits {
  implicit val GenderMeta: Meta[Gender] =
    pgEnumStringOpt("gender", Gender.fromEnum, Gender.toEnum)

  implicit val LangGet: Get[Language] = Get[String].tmap(Language.withName)
  implicit val LangPut: Put[Language] = Put[String].tcontramap(_.toString)

  implicit val StatusGet: Get[Status] = Get[String].tmap(Status.withName)
  implicit val StatusPut: Put[Status] = Put[String].tcontramap(_.toString)

  implicit val UuidGet: Get[UUID] = Get[String].tmap(UUID.fromString)
  implicit val UuidPut: Put[UUID] = Put[String].tcontramap(_.toString)

  implicit val TariffGet: Get[Tariff] = Get[String].tmap(Tariff.withName)
  implicit val TariffPut: Put[Tariff] = Put[String].tcontramap(_.toString)

  implicit val SeqGet: Get[Seq[Language]] = Get[List[String]].tmap(seq => seq.map(el => Language.withName(el)))
  implicit val SeqPut: Put[Seq[Language]] = Put[List[String]].tcontramap(seq => seq.map(el => el.toString).toList)
}

