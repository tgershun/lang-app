package ru.tinkoff.lang.logging

import cats.effect.IO
import org.slf4j._
import ru.tinkoff.lang.logging

object Log {
  def apply(clazz: Class[_]): logging.Log = new logging.Log(LoggerFactory.getLogger(clazz))
}

class Log(logger: Logger) {
  def info(message: String): IO[Unit] = IO(logger.info(message))
  def debug(message: String): IO[Unit] = IO(logger.debug(message))
  def error(message: String, ex: Throwable): IO[Unit] = IO(logger.error(message, ex))
  def error(message: String): IO[Unit] = IO(logger.error(message))
}
