package ru.tinkoff.lang

import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server._
import org.http4s.server.blaze._
import ru.tinkoff.lang.controller.{ClientController, ProfessorController}
import ru.tinkoff.lang.service.{AuthServiceImpl, ClientServiceImpl, PaymentServiceImpl, ProfessorServiceImpl}
import ru.tinkoff.lang.config.ConfigComponent
import ru.tinkoff.lang.config.ConfigComponent.DbConf
import ru.tinkoff.lang.exceptions.{ClientHandler, HttpErrorHandler, ProfessorHandler}
import ru.tinkoff.lang.exceptions.ServiceExceptions.{ClientException, ProfessorException}

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

object LangServer extends IOApp {

  implicit val clientErrorHandler: HttpErrorHandler[ClientException] = new ClientHandler()
  implicit val professorErrorHandler: HttpErrorHandler[ProfessorException] = new ProfessorHandler()

  override def run(args: List[String]): IO[ExitCode] = {
    createServer
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }

  private def createServer: Resource[IO, Server[IO]] = {
    for {
      serverEc <- ExecutionContexts.cachedThreadPool[IO]

      connEc <- ExecutionContexts.fixedThreadPool[IO](4)
      jdbcEc <- ExecutionContexts.fixedThreadPool[IO](4)

      conf <- ConfigComponent.apply()

      xa <- dbTransactor(conf.db, connEc, Blocker.liftExecutionContext(jdbcEc))
      client <- BlazeClientBuilder[IO](global).resource

      paymentService = new PaymentServiceImpl(client, s"http://${conf.payment.host}:${conf.payment.port}")
      authService = new AuthServiceImpl()
      clientService = new ClientServiceImpl(xa, paymentService, authService)
      professorService = new ProfessorServiceImpl(xa, paymentService, authService)

      clientController = new ClientController(clientService)
      professorController = new ProfessorController(professorService)

      server <- BlazeServerBuilder[IO](serverEc)
        .bindHttp(conf.server.port, conf.server.host)
        .withHttpApp(Router(
          "/client" -> clientController.routes,
          "/professor" -> professorController.routes
        ).orNotFound)
        .resource

    } yield server
  }

  private def dbTransactor(dbConf: DbConf, connEc: ExecutionContext, blocker: Blocker): Resource[IO, HikariTransactor[IO]] =
    HikariTransactor
      .newHikariTransactor[IO](dbConf.driver, dbConf.url, dbConf.user, dbConf.password, connEc, blocker)

}
