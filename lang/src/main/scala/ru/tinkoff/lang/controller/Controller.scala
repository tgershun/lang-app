package ru.tinkoff.lang.controller

import cats.effect.IO
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

trait Controller {
  val routes: HttpRoutes[IO]

}

abstract class Http4sController extends Controller with Http4sDsl[IO]

