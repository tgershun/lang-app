package ru.tinkoff.lang.controller

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, OffsetDateTime}

import org.http4s.QueryParamDecoder
import org.http4s.dsl.impl.{OptionalQueryParamDecoderMatcher, QueryParamDecoderMatcher}
import ru.tinkoff.lang.entity.{Language, Role}


trait DecoderImplicits {
  implicit val langQueryParamDecoder: QueryParamDecoder[Language] =
    QueryParamDecoder[String].map(Language.withName)

  implicit val roleQueryParamDecoder: QueryParamDecoder[Role] =
    QueryParamDecoder[String].map(Role.withName)

  implicit val decimalQueryParamDecoder: QueryParamDecoder[BigDecimal] =
    QueryParamDecoder[String].map(BigDecimal(_))

  implicit val localDateQueryParamDecoder: QueryParamDecoder[LocalDate] =
    QueryParamDecoder[String].map(LocalDate.parse(_, DateTimeFormatter.ISO_LOCAL_DATE))

  implicit val offsetDateTimeQueryParamDecoder: QueryParamDecoder[OffsetDateTime] =
    QueryParamDecoder[String].map(OffsetDateTime.parse(_, DateTimeFormatter.ISO_OFFSET_DATE_TIME))
}

trait CommonParameters extends DecoderImplicits {

  object UserId extends QueryParamDecoderMatcher[Int]("userId")

  object UserRole extends QueryParamDecoderMatcher[Role]("role")

  object LessonId extends QueryParamDecoderMatcher[Int]("lId")
}

object profParameters extends CommonParameters {

  object Age extends OptionalQueryParamDecoderMatcher[Int]("minAge")

  object IsNative extends OptionalQueryParamDecoderMatcher[Boolean]("isNative")

  object HasEducation extends OptionalQueryParamDecoderMatcher[Boolean]("hasEducation")

  object Lang extends QueryParamDecoderMatcher[Language]("lang")

  object Date extends QueryParamDecoderMatcher[LocalDate]("date")

  object StartTime extends QueryParamDecoderMatcher[OffsetDateTime]("startTime")

  object TimeSlot extends QueryParamDecoderMatcher[Int]("slotN")

  object MinScore extends OptionalQueryParamDecoderMatcher[BigDecimal]("minScore")

  object MaxPrice extends OptionalQueryParamDecoderMatcher[Int]("maxPrice")

  object Comment extends OptionalQueryParamDecoderMatcher[String]("comment")

  object Score extends QueryParamDecoderMatcher[Int]("score")

  object ClientId extends QueryParamDecoderMatcher[Int]("cId")

  object PaginationParameters {

    object Page extends QueryParamDecoderMatcher[Int]("page")

    object Size extends QueryParamDecoderMatcher[Int]("size")

  }

}

object clientParameters extends CommonParameters