package ru.tinkoff.lang.controller

import cats.effect.IO
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import ru.tinkoff.lang.controller.profParameters._
import ru.tinkoff.lang.entity.{Professor, Score}
import ru.tinkoff.lang.exceptions.HttpErrorHandler
import ru.tinkoff.lang.exceptions.ServiceExceptions.ProfessorException
import ru.tinkoff.lang.service.ProfessorService


class ProfessorController(professorService: ProfessorService)(implicit H: HttpErrorHandler[ProfessorException]) extends Http4sController {

  val httpRoutes: HttpRoutes[IO] = HttpRoutes.of[IO] {

    case req@POST -> Root / "register" =>
      for {
        professor <- req.as[Professor]
        professorWithGeneratedId <- professorService.register(professor)
        response <- Ok(professorWithGeneratedId.asJson)
      } yield response

    case req@POST -> Root / "update" / IntVar(id) :? UserId(uId) +& UserRole(role) =>
      for {
        professor <- req.as[Professor]
        pResponse <- professorService.update(id, professor, uId, role)
        response <- Ok(pResponse.asJson)
      } yield response

    case GET -> Root / "list" :? UserId(uId) +& UserRole(role) +& Age(age) +& IsNative(isNative) +& HasEducation(hasEducation) +& Lang(l) +& MinScore(minScore) +& MaxPrice(maxPrice)
      +& PaginationParameters.Page(page) +& PaginationParameters.Size(size) => for {
      professors <- professorService.list(l, age, isNative, hasEducation, maxPrice, minScore, page, size)
      response <- Ok(professors.asJson)
    } yield response

    case GET -> Root / IntVar(id) :? UserId(uId) +& UserRole(role) =>
      for {
        professor <- professorService.getById(id, uId, role)
        response <- Ok(professor.asJson)
      } yield response

    case GET -> Root / IntVar(id) / "schedule" :? UserId(uId) +& UserRole(role) +& Date(date) => for {
      lessons <- professorService.getSchedule(id, date, uId, role)
      response <- Ok(lessons.asJson)
    } yield response

    case req@POST -> Root / IntVar(id) / "rate" :? UserId(uId) +& UserRole(role) =>
      for {
        score <- req.as[Score]
        lessons <- professorService.rate(id, score, uId, role)
        response <- Ok(lessons.asJson)
      } yield response

    case POST -> Root / IntVar(id) / "requestLesson" :? UserId(uId) +& UserRole(role) +& Date(date) +& TimeSlot(slotN) =>
      for {
        lesson <- professorService.requestLesson(id, date, slotN, uId, role)
        response <- Ok(lesson.asJson)
      } yield response

    case GET -> Root / IntVar(id) / "listScores" :? UserId(uId) +& UserRole(role) +& PaginationParameters.Page(page) +& PaginationParameters.Size(size) => for {
      professors <- professorService.listScores(id, page, size, uId, role)
      response <- Ok(professors.asJson)
    } yield response

    case POST -> Root / "approveLesson" :? LessonId(lId) +& UserId(uId) +& UserRole(role) => for {
      lesson <- professorService.approveLesson(lId, uId, role)
      response <- Ok(lesson.asJson)
    } yield response

    case GET -> Root / "getLesson" :? LessonId(lId) +& UserId(uId) +& UserRole(role) => for {
      lesson <- professorService.getLesson(lId, uId, role)
      response <- Ok(lesson.asJson)
    } yield response
  }

  override val routes = H.handle(httpRoutes)
}
