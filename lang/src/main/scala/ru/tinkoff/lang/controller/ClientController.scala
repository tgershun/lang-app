package ru.tinkoff.lang.controller

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import ru.tinkoff.lang.entity.Client
import ru.tinkoff.lang.exceptions.{HttpErrorHandler}
import ru.tinkoff.lang.exceptions.ServiceExceptions.ClientException
import ru.tinkoff.lang.service.ClientService
import ru.tinkoff.lang.controller.clientParameters._


class ClientController(clientService: ClientService)(implicit H: HttpErrorHandler[ClientException]) extends Http4sController {

  val httpRoutes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case req@POST -> Root / "register" =>
      for {
        client <- req.as[Client]
        clientWithId <- clientService.register(client)
        response <- Ok(clientWithId.asJson)
      } yield response

    case req@POST -> Root / "update" / IntVar(id) :? UserId(uId) +& UserRole(role) => for {
      client <- req.as[Client]
      client <- clientService.update(id, client, uId, role)
      response <- Ok(client.asJson)
    } yield response

    case GET -> Root / IntVar(id) :? UserId(uId) +& UserRole(role) =>
      for {
        client <-  clientService.getById(id, uId, role)
        response <- Ok(client.asJson)
      } yield response

    case POST -> Root / "block" / IntVar(id) :? UserId(uId) +& UserRole(role) =>
      for {
        client <- clientService.blockClient(id, uId, role)
        response <- Ok(client.asJson)
      } yield response

    case POST -> Root / "pay" :? LessonId(lId) +& UserId(uId) +& UserRole(role) =>
      for {
        lesson <- clientService.payForLesson(lId, uId, role)
        response <- Ok(lesson.asJson)
      } yield response
  }

  override val routes = H.handle(httpRoutes)
}
