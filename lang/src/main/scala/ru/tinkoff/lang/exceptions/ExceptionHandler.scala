package ru.tinkoff.lang.exceptions

import cats.data.{Kleisli, OptionT}
import cats.effect.IO
import cats.syntax.applicativeError._
import cats.{ApplicativeError, MonadError}
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Request, Response}
import ru.tinkoff.lang.exceptions.ServiceExceptions.{ClientException, InvalidCard, LessonAlreadyPaid, LessonNotFound, NoLessonsPassed, NotAcceptedLesson, PaymentException, ProfessorException, AlreadyRated, TakenTimeSlot, UnauthorizedUser, UserNotFound}

object ErrorHandler {

  def apply[E <: Throwable](routes: HttpRoutes[IO])(handler: E => IO[Response[IO]])(implicit ev: ApplicativeError[IO, E]): HttpRoutes[IO] =
    Kleisli { req: Request[IO] =>
      OptionT {
        catsSyntaxApplicativeError(routes.run(req).value).handleErrorWith { e =>
          handler(e).map(Option(_))
        }
      }
    }
}

case class ErrorResponse(error: String)

object ErrorResponse {
  implicit val encoder: Encoder[ErrorResponse] = deriveEncoder
  implicit val errorResponseDecoder = jsonOf[IO, ErrorResponse]
}

trait HttpErrorHandler[E <: Throwable] {
  def handle(routes: HttpRoutes[IO]): HttpRoutes[IO]
}

class ClientHandler(implicit M: MonadError[IO, Throwable]) extends HttpErrorHandler[ClientException] with Http4sDsl[IO] {

  private val handler: Throwable => IO[Response[IO]] = {
    case UnauthorizedUser(m) => Forbidden(ErrorResponse(m).asJson)
    case InvalidCard(m) => PaymentRequired(ErrorResponse(m).asJson)
    case NotAcceptedLesson(m) => BadRequest(ErrorResponse(m).asJson)
    case LessonAlreadyPaid(m) => BadRequest(ErrorResponse(m).asJson)
    case PaymentException(m) => PaymentRequired(ErrorResponse(m).asJson)
    case UserNotFound(m) => NotFound(ErrorResponse(m).asJson)
    case ex => InternalServerError(ErrorResponse(ex.getMessage).asJson)
  }

  override def handle(routes: HttpRoutes[IO]): HttpRoutes[IO] =
    ErrorHandler(routes)(handler)
}

class ProfessorHandler(implicit M: MonadError[IO, Throwable]) extends HttpErrorHandler[ProfessorException] with Http4sDsl[IO] {

  private val handler: Throwable => IO[Response[IO]] = {
    case UnauthorizedUser(m) => Forbidden(ErrorResponse(m).asJson)
    case InvalidCard(m) => PaymentRequired(ErrorResponse(m).asJson)
    case NoLessonsPassed(m) => BadRequest(ErrorResponse(m).asJson)
    case AlreadyRated(m) => Conflict(ErrorResponse(m).asJson)
    case TakenTimeSlot(m) => Conflict(ErrorResponse(m).asJson)
    case UserNotFound(m) => NotFound(ErrorResponse(m).asJson)
    case LessonNotFound(m) => NotFound(ErrorResponse(m).asJson)
    case ex => InternalServerError(ErrorResponse(ex.getMessage).asJson)
  }

  override def handle(routes: HttpRoutes[IO]): HttpRoutes[IO] =
    ErrorHandler(routes)(handler)
}