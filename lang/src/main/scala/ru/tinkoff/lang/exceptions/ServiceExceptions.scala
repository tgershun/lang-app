package ru.tinkoff.lang.exceptions

import scala.util.control.NoStackTrace

object ServiceExceptions {

  sealed trait ClientException extends Exception

  //Client exceptions
  case class NotAcceptedLesson(message: String) extends ClientException with NoStackTrace

  case class LessonAlreadyPaid(message: String) extends ClientException with NoStackTrace

  case class PaymentException(message: String) extends ClientException with NoStackTrace


  //Professor exceptions
  sealed trait ProfessorException extends Exception

  case class TakenTimeSlot(message: String) extends ProfessorException with NoStackTrace

  case class AlreadyRated(message: String) extends ProfessorException with NoStackTrace

  case class NoLessonsPassed(message: String) extends ProfessorException with NoStackTrace

  case class LessonNotFound(message: String) extends ProfessorException with NoStackTrace

  // Common user exceptions
  case class InvalidCard(message: String) extends ClientException with ProfessorException with NoStackTrace

  case class UnauthorizedUser(message: String) extends ClientException with ProfessorException with NoStackTrace

  case class UserNotFound(message: String) extends ClientException with ProfessorException with NoStackTrace

}
