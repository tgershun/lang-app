package ru.tinkoff.lang.service.queries

import java.time.LocalDate
import java.util.UUID

import doobie.implicits._
import doobie.implicits.javatime._
import doobie.Update0
import doobie.util.Read
import doobie.util.query.Query0
import ru.tinkoff.lang.entity.{Client, Gender, Language, Tariff}
import ru.tinkoff.lang.entity.DomainImplicits._

object ClientQueries {
  implicit val clientRead: Read[Client] =
    Read[(Option[Int], String, String, Language, Seq[Language], UUID, Tariff, Option[Gender], Option[LocalDate], Boolean)].map { case (id, f, l, nl, ls, c, t, g, b, isA) => new Client(id, f, l, nl, ls, c, g, b, t, isA) }

  def insertClient(client: Client): Update0 = {
    sql"""INSERT INTO clients (first_name, last_name, native_language, langs, card_id, gender, birthday, tariff, is_active)
      VALUES (${client.firstName}, ${client.lastName}, ${client.nativeLanguage}, ${client.languagesToLearn}, ${client.cardId}, ${client.gender}::gender, ${client.birthday}, ${client.tariff}, ${client.isActive})"""
      .update
  }

  def updateClient(id: Int, client: Client): Update0 = {
    sql"""UPDATE clients SET (first_name, last_name, native_language, langs, card_id, gender, birthday, tariff, is_active)
      = (${client.firstName}, ${client.lastName}, ${client.nativeLanguage}, ${client.languagesToLearn}, ${client.cardId}, ${client.gender}::gender, ${client.birthday}, ${client.tariff}, ${client.isActive})
      where id = $id"""
      .update
  }

  def getClientByIdQuery(id: Int): Query0[Client] = {
    sql"""select id, first_name, last_name, native_language, langs, card_id, tariff, gender, birthday, is_active  from clients
      where id = $id and is_active = true"""
      .query[Client]
  }

  // think about money, forbit actions
  def blockClientQuery(id: Int): Update0 = {
    sql"""UPDATE clients SET is_active = false where id = $id"""
      .update
  }

}
