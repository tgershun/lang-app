package ru.tinkoff.lang.service

import cats.effect.IO
import ru.tinkoff.lang.entity.Role
import ru.tinkoff.lang.exceptions.ServiceExceptions.UnauthorizedUser
import ru.tinkoff.lang.logging.Log


trait AuthService {
  def checkPerms(givenRole: Role, expectedRole: Role, givenId: Int, expectedId: Option[Int]): IO[Unit]
}

class AuthServiceImpl extends AuthService {
  val logger = Log(classOf[AuthServiceImpl])
  val unauthorizedMessage = "User is not authorized"

  override def checkPerms(givenRole: Role, expectedRole: Role, givenId: Int, expectedId: Option[Int]): IO[Unit] = {
    for {
      _ <- logger.info(s"Checking permissions user $givenId with role $givenRole. Expectation is $expectedId:$expectedRole ")
      res <- expectedId match {
        case Some(expId) => isUserAuthorized(givenRole, expectedRole, givenId, expId)
        case _ => isRoleAuthorized(givenRole, expectedRole)
      }
      _ <- if (res) {
        logger.info(s"Permissions checked. User $givenId is authorized")
      } else {
        // needs to log action as well
        logger.error(s"Permission denied for user $givenId") *> IO.raiseError(UnauthorizedUser(unauthorizedMessage))
      }
    } yield ()
  }

  private def isRoleAuthorized(givenRole: Role, expectedRole: Role): IO[Boolean] = {
    for {
      _ <- logger.debug("Checking permissions - role")
      response <- IO(givenRole == expectedRole)
      _ <- logger.debug(s"Permissions check for role $givenRole gives $response")
    } yield response
  }

  private def isUserAuthorized(givenRole: Role, expectedRole: Role, givenId: Int, expectedId: Int): IO[Boolean] = {
    for {
      _ <- logger.debug(s"Checking permissions - user")
      response <- IO(givenId == expectedId && givenRole == expectedRole)
      _ <- logger.debug(s"Permissions check for user $givenId:$givenRole gives $response")
    } yield response
  }

}
