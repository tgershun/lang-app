package ru.tinkoff.lang.service.queries

import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.Read
import doobie.util.query.Query0
import doobie.util.update.Update0
import ru.tinkoff.lang.entity.Score
import ru.tinkoff.lang.entity.DomainImplicits._

object ScoreQueries {

  implicit val readScore: Read[Score] =
    Read[(Option[Int], Option[Int], Int, Option[String])].map { case (pId, cId, score, comment) => new Score(pId, cId, score, comment) }

  def updateProfessorsScore(id: Int): Update0 = {
    sql"""update professors set score = (select avg("score")::DECIMAL(3,2) from scores where professor_id = $id) where id = $id"""
      .update
  }

  def listScoresQuery(id: Int): Query0[Score] = {
    sql"""select professor_id, client_id, score, comment from scores
         where professor_id = $id"""
      .query[Score]
  }
}
