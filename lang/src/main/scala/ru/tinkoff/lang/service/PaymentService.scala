package ru.tinkoff.lang.service

import java.util.UUID

import cats.effect.IO
import cats.effect.IO._
import org.http4s.Method.POST
import org.http4s.Uri
import org.http4s.circe.jsonOf
import io.circe.generic.auto._
import org.http4s.client.Client
import org.http4s.client.dsl.io._
import ru.tinkoff.lang.exceptions.ErrorResponse
import ru.tinkoff.lang.exceptions.ServiceExceptions.{InvalidCard, PaymentException}
import ru.tinkoff.lang.logging.Log

trait PaymentService {
  def charge(card: UUID, amount: Int): IO[Unit]

  def checkCard(cardId: UUID): IO[Unit]
}

class PaymentServiceImpl(client: Client[IO], endpoint: String) extends PaymentService {

  val logger = Log(classOf[PaymentServiceImpl])

  case class IsValidResponse(response: Boolean)

  object IsValidResponse {
    implicit val isValidResponseDecoder = jsonOf[IO, IsValidResponse]
  }

  override def charge(fromCard: UUID, amount: Int): IO[Unit] = {
    for {
      _ <- logger.info(s"Charge amount $amount")
      uriS <- IO(s"""$endpoint/payment/charge?card=$fromCard&amount=$amount&platformToken=someToken""")
      uri <- Uri.fromString(uriS)
        .fold(ex => logger.error(s"Cannot create uri from string $uriS") *> IO.raiseError(ex), IO(_))
      _ <- client.expectOr[Unit](POST(uri)) {
        error =>
          error.as[ErrorResponse]
            .map(ex => PaymentException(ex.error))
      }
    } yield ()
  }

  override def checkCard(cardId: UUID): IO[Unit] = {
    for {
      _ <- logger.info(s"Check card")
      isValid <- isValid(cardId)
      _ <- if (!isValid) {
        logger.error("Card is invalid") *> IO.raiseError(InvalidCard("Card is invalid"))
      } else {
        logger.debug("Card is valid")
      }
    } yield ()
  }

  def isValid(card: UUID): IO[Boolean] = {
    for {
      uriS <- IO(s"""$endpoint/payment/checkCard?card=$card&platformToken=someToken""")
      uri <- Uri.fromString(uriS).fold(ex => logger.error(s"Cannot create uri from $uriS") *> IO.raiseError(ex), IO(_))
      res <- client.expectOr[IsValidResponse](POST(uri)) {
        error =>
          error.as[ErrorResponse]
            .map(ex => {
              PaymentException(ex.error)
            })
      }
    } yield res.response
  }
}
