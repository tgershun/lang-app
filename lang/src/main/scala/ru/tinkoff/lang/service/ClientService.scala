package ru.tinkoff.lang.service

import cats.effect.{IO, _}
import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.transactor.Transactor
import ru.tinkoff.lang.entity.{Client, Lesson, Role, Status}
import ru.tinkoff.lang.exceptions.ServiceExceptions.{LessonAlreadyPaid, NotAcceptedLesson, UserNotFound}
import ru.tinkoff.lang.logging.Log

trait ClientService {
  def register(client: Client): IO[Client]

  def update(id: Int, client: Client, userId: Int, role: Role): IO[Client]

  def getById(id: Int, userId: Int, role: Role): IO[Client]

  def blockClient(id: Int, userId: Int, role: Role): IO[Client]

  def payForLesson(lId: Int, userId: Int, role: Role): IO[Lesson]

}

class ClientServiceImpl(xa: Transactor[IO], paymentService: PaymentService, authService: AuthService) extends ClientService {

  import ru.tinkoff.lang.service.queries.ClientQueries._
  import ru.tinkoff.lang.service.queries.ProfessorQueries._
  import ru.tinkoff.lang.service.queries.LessonQueries._

  val logger = Log(classOf[ClientServiceImpl])

  override def register(client: Client): IO[Client] = {
    for {
      _ <- logger.info("Register client user")
      _ <- paymentService.checkCard(client.cardId)

      response <- insertClient(client)
        .withUniqueGeneratedKeys[Client]("id", "first_name", "last_name", "native_language", "langs", "card_id", "tariff", "gender", "birthday", "is_active")
        .transact(xa)
    } yield response
  }

  override def update(id: Int, client: Client, userId: Int, role: Role): IO[Client] = {
    for {
      _ <- logger.info(s"Update client user with id $id")
      _ <- authService.checkPerms(role, Role.Client, userId, Some(id))
      _ <- paymentService.checkCard(client.cardId)

      response <- updateClient(id, client)
        .withUniqueGeneratedKeys[Client]("id", "first_name", "last_name", "native_language", "langs", "card_id", "tariff", "gender", "birthday", "is_active")
        .transact(xa)
    } yield response
  }

  override def getById(id: Int, userId: Int, role: Role): IO[Client] = {
    for {
      _ <- logger.info(s"Get client by id $id")

      client <- getClientByIdQuery(id).option.transact(xa)
      response <- if (client.isEmpty) IO.raiseError(UserNotFound(s"Client with id $id wasn't found")) else IO.pure(client.get)
    } yield response
  }

  override def blockClient(id: Int, userId: Int, role: Role): IO[Client] = {
    for {
      _ <- logger.info(s"Update client user with id $id")
      _ <- authService.checkPerms(role, Role.Admin, userId, None)

      response <- blockClientQuery(id)
        .withUniqueGeneratedKeys[Client]("id", "first_name", "last_name", "native_language", "langs", "card_id", "tariff", "gender", "birthday", "is_active")
        .transact(xa)
    } yield response
  }

  override def payForLesson(lId: Int, userId: Int, role: Role): IO[Lesson] = {
    for {
      _ <- logger.info(s"Pay for lesson $lId")
      lcp <- (for {
        lesson <- getLessonByIdQuery(lId).unique
        client <- getClientByIdQuery(userId).unique
        prof <- getProfessorByIdQuery(lesson.professorId).unique
      } yield (lesson, client, prof))
        .transact(xa)

      _ <- authService.checkPerms(role, Role.Client, userId, Some(lcp._1.clientId))
      _ <- if (lcp._1.status != Status.Accepted) {
        logger.error("Lesson is not accepted") *> IO.raiseError(NotAcceptedLesson("Lesson is not accepted yet."))
      } else if (lcp._1.isPaid) {
        logger.error("Lesson is already paid") *> IO.raiseError(LessonAlreadyPaid("You can not pay twice."))
      } else IO.unit

      _ <- paymentService.charge(lcp._2.cardId, lcp._3.pricePerLesson).handleErrorWith(ex => logger.error(s"Cannot charge ${lcp._3.pricePerLesson} rub for lesson $lId from user $userId") *> IO.raiseError(ex))

      // In 1 transaction
      lesson <- (for {
        lesson <- markLessonAsPaidQuery(lId).withUniqueGeneratedKeys[Lesson]("id", "professor_id", "date", "time_slot_id", "client_id", "is_paid", "status", "comment")
        _ <- upsertVirtMoneyInfo(lcp._3.id.get, lcp._3.pricePerLesson).withUniqueGeneratedKeys[Int]("pid")
      } yield lesson).transact(xa)

    } yield lesson
  }
}
