package ru.tinkoff.lang.service.queries

import java.time.LocalDate

import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.Read
import doobie.util.query.Query0
import doobie.util.update.Update0
import ru.tinkoff.lang.entity.{Lesson, Status}
import ru.tinkoff.lang.entity.DomainImplicits._

object LessonQueries {
  implicit val readLesson: Read[Lesson] =
    Read[(Option[Int], Int, LocalDate, Int, Int, Boolean, Status, Option[String])].map { case (id, pId, ld, ts, cId, s, isP, c) => new Lesson(id, pId, ld, ts, cId, isP, s, c) }


  def getScheduleQuery(id: Int, date: LocalDate): Query0[Lesson] = {
    sql"""select id, professor_id, date, time_slot_id, client_id, is_paid, status, comment from schedule
          where date = $date and professor_id = $id"""
      .query[Lesson]
  }

  def requestLessonQuery(id: Int, date: LocalDate, timeSlot: Int, clientId: Int, isPaid: Boolean, status: Status, comment: Option[String]): Update0 = {
    sql"""INSERT INTO schedule (professor_id, date, time_slot_id, client_id, is_paid, status, comment)
      VALUES ($id, $date, $timeSlot, $clientId, $isPaid, $status, $comment)""".update
  }

  def setLessonStatusQuery(lId: Int, status: Status): Update0 = {
    println(sql"""update schedule set status = $status where id = $lId""".toString())
    sql"""update schedule set status = $status where id = $lId"""
      .update
  }

  def markLessonAsPaidQuery(lId: Int): Update0 = {
    sql"""update schedule set is_paid = true where id = $lId"""
      .update
  }

  def getLessonByIdQuery(lId: Int): Query0[Lesson] = {
    sql"""select id, professor_id, date, time_slot_id, client_id, is_paid, status, comment from schedule
          where id = $lId"""
      .query[Lesson]
  }
}
