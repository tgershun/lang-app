package ru.tinkoff.lang.service.queries

import java.time.LocalDate
import java.util.UUID

import doobie.Fragment
import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.Read
import doobie.util.fragments.whereAndOpt
import doobie.util.query.Query0
import doobie.util.update.Update0
import ru.tinkoff.lang.entity.{Gender, Language, Professor, Score}
import ru.tinkoff.lang.entity.DomainImplicits._

object ProfessorQueries {
  implicit val readProfessor: Read[Professor] =
    Read[(Option[Int], String, String, Language, Boolean, UUID, Int, Option[Gender], Option[LocalDate], Seq[Language], Option[BigDecimal], Boolean)].map { case (id, f, l, nl, ed, c, p, g, b, sl, s, isA) => new Professor(id, f, l, nl, sl, ed, c, p, g, b, s, isA) }

  def insertProfessor(professor: Professor): Update0 = {
    sql"""INSERT INTO professors (first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score, is_active)
      VALUES (${professor.firstName}, ${professor.lastName}, ${professor.nativeLanguage}, ${professor.languagesToTeach}, ${professor.cardId}, ${professor.pricePerLesson}, ${professor.gender}::gender, ${professor.birthday}, ${professor.hasEducation}, ${professor.score}, ${professor.isActive})"""
      .update
  }

  def upsertVirtMoneyInfo(pId: Int, virtMoney: Int): Update0 = {
    sql"""INSERT INTO virt_money as v (pid, money) VALUES ($pId, $virtMoney)
          ON CONFLICT (pid) DO UPDATE SET money = v.money::int + $virtMoney"""
      .update
  }

  def updateProfessor(id: Int, professor: Professor): Update0 = {
    sql"""UPDATE professors SET (first_name, last_name, native_language, langs, card_id, price_per_lesson, gender, birthday, has_education, score)
      = (${professor.firstName}, ${professor.lastName}, ${professor.nativeLanguage}, ${professor.languagesToTeach}, ${professor.cardId}, ${professor.pricePerLesson}, ${professor.gender}::gender, ${professor.birthday}, ${professor.hasEducation}, ${professor.score})
      where id = $id and is_active = true"""
      .update
  }

  def getProfessorByIdQuery(id: Int): Query0[Professor] = {
    sql"""select id, first_name, last_name, native_language, has_education, card_id, price_per_lesson, gender, birthday, langs, score, is_active from professors
      where id = $id and is_active = true"""
      .query[Professor]
  }

  def listProfessorsQuery(lang: Language, age: Option[Int], isNative: Option[Boolean],
                          hasPedagogicalEducation: Option[Boolean], price: Option[Int], score: Option[BigDecimal]): Query0[Professor] = {
    val ageFilter = age.map(a => Fragment.const(s"""age("birthday") >= '$a years'"""))
    val priceFilter = price.map(p => Fragment.const(s"""price_per_lesson <= $p"""))
    val scoreFilter = score.map(s => Fragment.const(s"""score >= $s"""))
    val isActiveFilter = fr"""is_active = true"""

    val isNativeFilter = isNative.map {
      case true => fr"native_language = $lang"
      case false => fr"native_language != $lang"
    }

    val hasEducationFilter = hasPedagogicalEducation.map(bool => fr"has_education = $bool")

    val langFilter = Fragment.const(s"langs @> ARRAY['$lang']::varchar[]")

    val query = fr"select id, first_name, last_name, native_language, has_education, card_id, price_per_lesson, gender, birthday, langs, score, is_active from professors" ++
      whereAndOpt(ageFilter, isNativeFilter, hasEducationFilter, scoreFilter, priceFilter, Some(langFilter), Some(isActiveFilter))

    query.query[Professor]
  }

  def insertScoreQuery(id: Int, clientId: Int, score: Score): Update0 = {
    sql"""INSERT INTO scores (professor_id, client_id, score, comment)
      VALUES ($id, $clientId, ${score.score}, ${score.comment})""".update
  }

  def lessonsCountQuery(id: Int, clientId: Int): Query0[Int] = {
    sql"""select count(*)::int from schedule
          where professor_id = $id and client_id = $clientId and date < ${LocalDate.now}"""
      .query[Int]
  }

}
