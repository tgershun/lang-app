package ru.tinkoff.lang.service

import java.time.LocalDate

import cats.effect.IO
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.implicits.javatime._
import doobie.postgres.sqlstate
import doobie.util.transactor.Transactor
import org.postgresql.util.PSQLException
import ru.tinkoff.lang.entity.{Language, Lesson, Professor, Role, Score, Status}
import ru.tinkoff.lang.entity.Status.Requested
import ru.tinkoff.lang.entity.DomainImplicits._
import ru.tinkoff.lang.exceptions.ServiceExceptions.{LessonNotFound, NoLessonsPassed, AlreadyRated, TakenTimeSlot, UserNotFound}
import ru.tinkoff.lang.logging.Log

trait ProfessorService {
  def register(professor: Professor): IO[Professor]

  def update(id: Int, professor: Professor, userId: Int, role: Role): IO[Professor]

  def getById(id: Int, userId: Int, role: Role): IO[Professor]

  def list(lang: Language, age: Option[Int], isNative: Option[Boolean],
           hasEducation: Option[Boolean], price: Option[Int], score: Option[BigDecimal], page: Int, pageSize: Int): IO[Seq[Professor]]

  def approveLesson(lId: Int, userId: Int, role: Role): IO[Lesson]

  def getLesson(lId: Int, userId: Int, role: Role): IO[Lesson]

  def getSchedule(teacherId: Int, localDate: LocalDate, userId: Int, role: Role): IO[Seq[Lesson]]

  def requestLesson(id: Int, date: LocalDate, timeSlot: Int, userId: Int, role: Role): IO[Lesson]

  def rate(id: Int, score: Score, userId: Int, role: Role): IO[Score]

  def listScores(id: Int, page: Int, pageSize: Int, userId: Int, role: Role): IO[Seq[Score]]
}

class ProfessorServiceImpl(xa: Transactor[IO], paymentService: PaymentService, authService : AuthService) extends ProfessorService {

  import ru.tinkoff.lang.service.queries.ProfessorQueries._
  import ru.tinkoff.lang.service.queries.LessonQueries._
  import ru.tinkoff.lang.service.queries.ScoreQueries._

  val logger = Log(classOf[ProfessorServiceImpl])

  override def register(professor: Professor): IO[Professor] = {
    for {
      _ <- logger.info("Register professor user")
      _ <- paymentService.checkCard(professor.cardId)

      id <- insertProfessor(professor)
        .withUniqueGeneratedKeys[Professor]("id", "first_name", "last_name", "native_language", "has_education", "card_id", "price_per_lesson", "gender", "birthday", "langs", "score", "is_active")
        .transact(xa)
    } yield id
  }

  override def update(id: Int, professor: Professor, userId: Int, role: Role): IO[Professor] = {
    for {
      _ <- logger.info(s"Update professor user with id $id")

      _ <- authService.checkPerms(role, Role.Instructor, userId, Some(id))
      _ <- paymentService.checkCard(professor.cardId)

      id <- updateProfessor(id, professor)
        .withUniqueGeneratedKeys[Professor]("id", "first_name", "last_name", "native_language", "has_education", "card_id", "price_per_lesson", "gender", "birthday", "langs", "score", "is_active")
        .transact(xa)
    } yield id
  }

  override def list(lang: Language, age: Option[Int], isNative: Option[Boolean],
                    hasPedagogicalEducation: Option[Boolean], price: Option[Int], score: Option[BigDecimal], page: Int, pageSize: Int): IO[Seq[Professor]] = {
    for {
      _ <- logger.info(s"Search for teachers")

      result <- listProfessorsQuery(lang, age, isNative, hasPedagogicalEducation, price, score).stream
        .drop(page * pageSize)
        .take(pageSize)
        .compile
        .toVector
        .transact(xa)
    } yield result
  }

  override def getSchedule(id: Int, localDate: LocalDate, userId: Int, role: Role): IO[Seq[Lesson]] = {
    for {
      _ <- logger.info(s"Get schedule for professor with id $id and date $localDate")

      lessons <- getScheduleQuery(id, localDate)
        .stream
        .compile
        .toVector
        .transact(xa)
    } yield lessons
  }

  override def requestLesson(id: Int, date: LocalDate, timeSlot: Int, userId: Int, role: Role): IO[Lesson] = {
    for {
      _ <- logger.info(s"Request lesson. client $userId request lesson from professor $id lesson on $date with timeslot $timeSlot")
      _ <- authService.checkPerms(role, Role.Client, userId, None)

      lesson <- requestLessonQuery(id, date, timeSlot, userId, false, Requested, None)
        .withUniqueGeneratedKeys("id", "professor_id", "date", "time_slot_id", "client_id", "is_paid", "status", "comment")(readLesson)
        .transact(xa)
        .handleErrorWith {
          case ex: PSQLException if ex.getSQLState.equalsIgnoreCase(sqlstate.class23.UNIQUE_VIOLATION.value) =>
            logger.error(s"Timeslot $timeSlot is not available on $date") *> IO.raiseError(TakenTimeSlot("Timeslot is not available anymore"))
          case ex => logger.error(s"Something went wrong while requesting timeslot $timeSlot on $date", ex) *> IO.raiseError(ex)
        }
    } yield lesson
  }

  override def approveLesson(lId: Int, userId: Int, role: Role): IO[Lesson] = {
    for {
      _ <- logger.info(s"Approve lesson. lesson $lId userId $userId")
      lesson <- getLessonByIdQuery(lId).unique.transact(xa)

      _ <- authService.checkPerms(role, Role.Instructor, userId, Some(lesson.professorId))

      id <- setLessonStatusQuery(lId, Status.Accepted)
        .withUniqueGeneratedKeys[Lesson]("id", "professor_id", "date", "time_slot_id", "client_id", "is_paid", "status", "comment")
        .transact(xa)
    } yield id
  }

  override def getById(id: Int, userId: Int, role: Role): IO[Professor] = {
    for {
      _ <- logger.info(s"Get professor by id $id")

      professor <- getProfessorByIdQuery(id).option.transact(xa)
      response <- if (professor.isEmpty) IO.raiseError(UserNotFound(s"Professor with id $id wasn't found")) else IO.pure(professor.get)
    } yield response
  }

  override def rate(id: Int, score: Score, userId: Int, role: Role): IO[Score] = {
    for {
      _ <- logger.info(s"Rate. professor $id, user $userId with role $Role")
      _ <- authService.checkPerms(role, Role.Client, userId, None)
      count <- lessonsCountQuery(id, userId).unique.transact(xa)
      score <- count match {
        case 0 => logger.error(s"No lessons passed. user $userId, professor $id") *> IO.raiseError(NoLessonsPassed("No lessons passed. Sorry, you can not rate"))
        case _ => insertAndUpdateRate(id, userId, score).transact(xa)
          .handleErrorWith {
            case ex: PSQLException if ex.getSQLState.equalsIgnoreCase(sqlstate.class23.UNIQUE_VIOLATION.value) => logger.error(s"Professor $id is already rated by user $userId") *> IO.raiseError(AlreadyRated("Professor's already rated"))
            case ex => logger.error(s"Something went wrong while rating professor $id by user $userId") *> IO.raiseError(ex)
          }
      }
    } yield score
  }

  override def listScores(id: Int, page: Int, pageSize: Int, userId: Int, role: Role): IO[Seq[Score]] = {
    for {
      _ <- logger.info(s"Get scores by professor id $id")

      list <- listScoresQuery(id)
        .stream
        .compile
        .toVector
        .transact(xa)
    } yield list
  }

  private def insertAndUpdateRate(id: Int, clientId: Int, score: Score): ConnectionIO[Score] = {
    for {
      score <- insertScoreQuery(id, clientId, score)
        .withUniqueGeneratedKeys("professor_id", "client_id", "score", "comment")(readScore)
      _ <- updateProfessorsScore(id).withUniqueGeneratedKeys[Int]("id")
    } yield score
  }

  override def getLesson(lId: Int, userId: Int, role: Role): IO[Lesson] = {
    for {
      _ <- logger.info(s"Get lesson. lesson $lId")
      lesson <- getLessonByIdQuery(lId).option.transact(xa)
      response <- if (lesson.isEmpty) {
        logger.info(s"Lesson with id $lId wasn't found") *> IO.raiseError(LessonNotFound(s"Lesson wasn't found"))
      } else IO.pure(lesson.get)
    } yield response
  }
}
