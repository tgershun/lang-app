FROM openjdk:8

ENV SCALA_VERSION 2.12.11
ENV SBT_VERSION 1.3.10

# Install Scala
RUN \
  curl -fsL https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /root/ && \
  echo >> /root/.bashrc && \
  echo "export PATH=~/scala-$SCALA_VERSION/bin:$PATH" >> /root/.bashrc

# Install sbt
RUN curl -L -o sbt-$SBT_VERSION.deb "https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb" && \
  dpkg -i "sbt-${SBT_VERSION}.deb" && \
  rm "sbt-${SBT_VERSION}.deb"

COPY . /lang-app

