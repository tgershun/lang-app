package ru.tinkoff.payment.logging

import cats.effect.IO
import org.slf4j._

object Log {
  def apply(clazz: Class[_]): Log = new Log(LoggerFactory.getLogger(clazz))
}

class Log(logger: Logger) {
  def info(message: String): IO[Unit] = IO(logger.info(message))
  def debug(message: String): IO[Unit] = IO(logger.debug(message))
  def error(message: String, ex: Throwable): IO[Unit] = IO(logger.error(message, ex))
  def error(message: String): IO[Unit] = IO(logger.error(message))
}
