package ru.tinkoff.payment

import cats.effect._
import org.http4s.implicits._
import org.http4s.server._
import org.http4s.server.blaze._
import ru.tinkoff.payment.config.ConfigComponent
import ru.tinkoff.payment.controller._
import ru.tinkoff.payment.service.FakePaymentServiceImpl

import scala.concurrent.ExecutionContext.global

object PaymentServer extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    createServer
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }

  private def createServer: Resource[IO, Server[IO]] = {
    for {
      conf <- ConfigComponent.apply()

      paymentService = new FakePaymentServiceImpl
      paymentController = new FakePaymentController(paymentService)

      server <- BlazeServerBuilder[IO](global)
        .bindHttp(conf.server.port, conf.server.host)
        .withHttpApp(Router(
          "/payment" -> paymentController.routes
        ).orNotFound)
        .resource

    } yield server
  }

}
