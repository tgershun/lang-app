package ru.tinkoff.payment.service

import java.util.UUID

import cats.effect.IO
import ru.tinkoff.payment.logging.Log

import scala.util.control.NoStackTrace

trait FakePaymentService {
  def charge(card: UUID, amount: Int, token: String): IO[Unit]

  def isValidCard(card: UUID, token: String): IO[Boolean]
}

class NotEnoughMoneyException(message: String) extends Exception(message) with NoStackTrace

class FakePaymentServiceImpl extends FakePaymentService {
  private val invalidCard = UUID.fromString("b83c0138-64ab-4104-87ba-e32dc2e13914")
  private val notEnoughMoney = UUID.fromString("0753366a-e60f-4267-9377-699b3a2b4557")

  val logger = Log(classOf[FakePaymentServiceImpl])

  override def charge(card: UUID, amount: Int, token: String): IO[Unit] = {
    for {
      _ <- logger.info(s"Payment service. Charge")

      _ <- if (card == notEnoughMoney) {
        logger.error(s"Not enough money") *> IO.raiseError(new NotEnoughMoneyException("Low balance"))
      } else {
        IO.unit
      }
    } yield ()
  }

  override def isValidCard(card: UUID, token: String): IO[Boolean] = {
    for {
      _ <- logger.info("Payment service. Check card")
      res <- IO(card != invalidCard)
    } yield res
  }
}

