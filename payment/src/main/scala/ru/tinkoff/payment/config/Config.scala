package ru.tinkoff.payment.config

import cats.effect.{IO, Resource}
import pureconfig.ConfigSource
import pureconfig._
import pureconfig.generic.auto._

object ConfigComponent {

  final case class ServerConfig(host: String, port: Int)

  final case class ApplicationConfig(server: ServerConfig)

  def apply(): Resource[IO, ApplicationConfig] = {
    Resource.liftF[IO, ApplicationConfig](IO.pure(ConfigSource.default.loadOrThrow[ApplicationConfig]))
  }
}