package ru.tinkoff.payment.controller

import java.util.UUID

import cats.effect.IO
import ru.tinkoff.payment.service.{FakePaymentService, NotEnoughMoneyException}
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import io.circe.generic.auto._

class FakePaymentController(paymentService: FakePaymentService) extends Http4sController {

  object Card extends QueryParamDecoderMatcher[UUID]("card")

  object PlatformToken extends QueryParamDecoderMatcher[String]("platformToken")

  object Amount extends QueryParamDecoderMatcher[Int]("amount")

  case class ErrorResponse(error: String)
  case class BoolResponse(response: Boolean)

  override val routes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case POST -> Root / "charge" :? Card(card) +& Amount(amount) +& PlatformToken(token) =>
      (for {
        _ <- paymentService.charge(card, amount, token)
        response <- Ok()
      } yield response)
        .handleErrorWith {
          case ex: NotEnoughMoneyException => NotAcceptable(ErrorResponse("Not enough money").asJson)
        }

    case POST -> Root / "checkCard" :? Card(card) +& PlatformToken(token) =>
      for {
        res <- paymentService.isValidCard(card, token)
        response <- Ok(BoolResponse(res).asJson)
      } yield response
  }
}
