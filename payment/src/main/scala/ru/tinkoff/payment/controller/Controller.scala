package ru.tinkoff.payment.controller

import java.util.UUID

import cats.effect.IO
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, QueryParamDecoder}

trait Controller {
  val routes: HttpRoutes[IO]

  implicit val uuidQueryParamDecoder: QueryParamDecoder[UUID] =
    QueryParamDecoder[String].map(UUID.fromString)
}

abstract class Http4sController extends Controller with Http4sDsl[IO]

