### Pull and run the image
docker pull postgres:alpine
docker run --name tinkoff-language-postgres -p 5432:5432 -e POSTGRES_PASSWORD=somePass -e POSTGRES_USER=foreign-language-user -e POSTGRES_DB=foreign-language -d postgres:alpine

Set up your env:

export LANG_DB_PASS=...

From the root directory of the project execute

docker-compose build

Then from the payment/lang directory execute

docker-compose up

### Create schema
su - postgres
psql -U foreign-language-user -d foreign-language

CREATE TYPE gender AS ENUM ('female', 'male');

CREATE TABLE clients(
    id serial PRIMARY KEY  NOT NULL, 
    first_name VARCHAR (50) NOT NULL, 
    last_name VARCHAR (50) NOT NULL, 
    native_language VARCHAR (50) NOT NULL, 
    langs  VARCHAR(50)[] NOT NULL, 
    card_id VARCHAR (50) NOT NULL, 
    tariff VARCHAR (50) NOT NULL, 
    is_active boolean NOT NUll,
    gender gender, 
    birthday DATE
);

CREATE TABLE professors(
	id serial PRIMARY KEY  NOT NULL, 
	first_name VARCHAR (50) NOT NULL, 
	last_name VARCHAR (50) NOT NULL, 
	native_language VARCHAR (50) NOT NULL, 
	langs  VARCHAR(50)[] NOT NULL, 
	card_id VARCHAR (50) NOT NULL, 
	has_education boolean NOT NUll,
	price_per_lesson INT NOT NULL, 
	score DECIMAL(3,2),
	is_active boolean NOT NUll,
	gender gender, 
	birthday DATE
);

CREATE TABLE virt_money(
	pid int PRIMARY KEY  NOT NULL, 
	money int NOT NULL,
	FOREIGN KEY (pid) REFERENCES professors(id)
);

CREATE TABLE scores(
	professor_id int NOT NULL, 
	client_id int NOT NULL, 
	score int NOT NULL,
	comment VARCHAR (50),
	FOREIGN KEY (professor_id) REFERENCES professors(id),
    FOREIGN KEY (client_id) REFERENCES clients(id),
	PRIMARY KEY(professor_id, client_id)
);

CREATE TABLE schedule(
    id SERIAL,
	professor_id int  NOT NULL, 
	date DATE  NOT NULL, 
	time_slot_id int NOT NULL,
	client_id int  NOT NULL, 
	is_paid boolean NOT NULL,
	status VARCHAR (50) NOT NULL,
	comment VARCHAR (256),
	FOREIGN KEY (professor_id) REFERENCES professors(id),
	FOREIGN KEY (client_id) REFERENCES clients(id),
	PRIMARY KEY(professor_id, date, time_slot_id),
	UNIQUE (id)
);

### Drop schema

DROP TABLE IF EXISTS schedule;
DROP TABLE IF EXISTS scores;
DROP TABLE IF EXISTS virt_money;
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS professors;
drop type gender;

### Endpoints withput bodies

localhost:8080/client/register

localhost:8080/client/1?userId=1&role=Client

localhost:8080/client/update/1?userId=1&role=Client

localhost:8080/client/block/1?userId=1&role=Admin

localhost:8080/professor/register

localhost:8080/professor/update/1?userId=1&role=Instructor

localhost:8080/professor/1?userId=1&role=Client

localhost:8080/professor/list?minAge=30&lang=English&isNative=true&hasEducation=true&maxPrice=800&minScore=3.1&userId=1&role=Client&page=0&size=1

localhost:8080/professor/1/rate?userId=1&role=Client

localhost:8080/professor/1/listScores?page=0&size=4&userId=2&role=Client

localhost:8080/professor/1/requestLesson?userId=1&role=Client&date=2020-05-30&slotN=6

localhost:8080/professor/approveLesson?lId=2&userId=1&role=Instructor

localhost:8080/professor/getLesson?lId=2&userId=1&role=Instructor

localhost:8080/professor/1/schedule?date=2020-05-30&userId=1&role=Client

localhost:8080/client/pay?lId=2&userId=1&role=Client
